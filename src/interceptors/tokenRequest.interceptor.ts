import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { decode } from 'jsonwebtoken';

@Injectable()
export class AddTokenRequestInterceptor implements NestInterceptor {
  constructor() {}

  async intercept(context: ExecutionContext, next: CallHandler): Promise<any> {
    const req = context.switchToHttp().getRequest();

    const token = req.headers.authorization;
    if (!token)
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);

    const sanitizedToken = token.replace(/^bearer/i, '').trim();

    const decodedToken = decode(sanitizedToken);

    // use by your own

    return next.handle();
  }
}
