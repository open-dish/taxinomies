import { Module } from "@nestjs/common";
import { AppController } from "@controllers/app.controller";

import { ConfigurationModule } from "@config/config.module";
import { MedicationController } from "@controllers/medication.controller";
import { MedicationRepository } from "@repositories/medications.repository";
import { ObservationsRepository } from "@repositories/observations.repository";
import { ObservationsController } from "@controllers/observations.controller";
import { SnomedRepository } from "@repositories/snomed.repository";
import { SnomedController } from "@controllers/snomed.controller";

const repositories = [MedicationRepository, ObservationsRepository, SnomedRepository];
const services = [];
const controllers = [AppController, MedicationController, ObservationsController, SnomedController];
const providers = [];

@Module({
  imports: [ConfigurationModule],
  controllers: [...controllers],
  providers: [...services, ...repositories, ...providers],
})
export class AppModule {}
