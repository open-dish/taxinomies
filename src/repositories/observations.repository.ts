import { Injectable } from "@nestjs/common";
import { PrismaConfig } from "@config/prisma/prisma.client";
import { observations, Prisma } from "@prisma/client";
import { ObservationsRequest } from "src/models/requests/observation.request";

@Injectable()
export class ObservationsRepository {
  constructor(private prisma: PrismaConfig) {}

  async getOne(obsWhereUniqueInput: {
    LOINC_NUM: string;
    COMPONENT: string;
  }): Promise<observations[] | null> {
    return this.prisma.observations.findMany({
      where: {
        LOINC_NUM: obsWhereUniqueInput.LOINC_NUM,
        COMPONENT: {
          contains: obsWhereUniqueInput.COMPONENT,
        },
      },
    });
  }

  async getSome(obsWhereUniqueInput: { keyword: string }): Promise<any> {
    return this.prisma.observations.findMany({
      where: {
        OR: [
          {
            COMPONENT: {
              contains: obsWhereUniqueInput.keyword,
            },
          },
          {
            LONG_COMMON_NAME: {
              contains: obsWhereUniqueInput.keyword,
            },
          },
          {
            RELATEDNAMES2: {
              contains: obsWhereUniqueInput.keyword,
            },
          },
          {
            SYSTEM1: {
              contains: obsWhereUniqueInput.keyword,
            },
          },
          {
            DefinitionDescription: {
              contains: obsWhereUniqueInput.keyword,
            },
          },
          {
            SHORTNAME: {
              contains: obsWhereUniqueInput.keyword,
            },
          },
          {
            DisplayName: {
              contains: obsWhereUniqueInput.keyword,
            },
          },
        ],
      },
      select: {
        LOINC_NUM: true,
        COMPONENT: true,
        LONG_COMMON_NAME: true,
        RELATEDNAMES2: true,
        SYSTEM1: true,
        DefinitionDescription: true,
        SHORTNAME: true,
        DisplayName: true,
      },
    });
  }

  async getAll(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.observationsWhereUniqueInput;
    where?: Prisma.observationsWhereInput;
    orderBy?: Prisma.observationsOrderByWithRelationInput;
  }): Promise<observations[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.observations.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async create(data: ObservationsRequest): Promise<observations> {
    return this.prisma.observations.create({
      data,
    });
  }

  async update(params: {
    where: Prisma.observationsWhereUniqueInput;
    data: Prisma.observationsUpdateInput;
  }): Promise<observations> {
    const { where, data } = params;
    return this.prisma.observations.update({
      data,
      where,
    });
  }

  async delete(
    where: Prisma.observationsWhereUniqueInput
  ): Promise<observations> {
    return this.prisma.observations.delete({
      where,
    });
  }
}
