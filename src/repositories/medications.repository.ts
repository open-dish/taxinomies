import { Injectable } from '@nestjs/common';
import { PrismaConfig } from '@config/prisma/prisma.client';
import { medication, Prisma } from '@prisma/client';
import { MedicationRequest } from 'src/models/requests/medication.request';

@Injectable()
export class MedicationRepository {
  constructor(private prisma: PrismaConfig) {}

  async getOne(
    medWhereUniqueInput: { RXCUI: string, STR?: string },
  ): Promise<medication[] | null> {
    return this.prisma.medication.findMany({
      where: {
        STR: {
          contains: medWhereUniqueInput.STR
        },
        RXCUI: medWhereUniqueInput.RXCUI
      },
    });
  }

  async getAll(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.medicationWhereUniqueInput;
    where?: Prisma.medicationWhereInput;
    orderBy?: Prisma.medicationOrderByWithRelationInput;
  }): Promise<medication[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.medication.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async create(data: MedicationRequest): Promise<medication> {
    return this.prisma.medication.create({
      data,
    });
  }

  async update(params: {
    where: Prisma.medicationWhereUniqueInput;
    data: Prisma.medicationUpdateInput;
  }): Promise<medication> {
    const { where, data } = params;
    return this.prisma.medication.update({
      data,
      where,
    });
  }

  async delete(where: Prisma.medicationWhereUniqueInput): Promise<medication> {
    return this.prisma.medication.delete({
      where,
    });
  }
}
