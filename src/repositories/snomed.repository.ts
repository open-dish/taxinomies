import { Injectable } from "@nestjs/common";
import { PrismaConfig } from "@config/prisma/prisma.client";
import { snap_description } from "@prisma/client";

@Injectable()
export class SnomedRepository {
  constructor(private prisma: PrismaConfig) {}

  async getOne(data: { term: string }): Promise<snap_description[]> {
    return this.prisma.snap_description.findMany({
      take: 100,
      where: {
        term: {
          contains: data.term,
        },
      },
    });
  }

  async getByModule(data: { id: string }): Promise<snap_description[]> {
    return this.prisma.snap_description.findMany({
      take: 100,
      where: {
        OR: [
          {
            moduleId: BigInt(data.id),
          },
          {
            conceptId: BigInt(data.id),
          },
        ],
      },
      orderBy: [
        {
          conceptId: "asc",
        },
        { caseSignificanceId: "asc" },
      ],
    });
  }
}
