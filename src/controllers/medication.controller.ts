import { Body, Controller, Get, Post, Query } from "@nestjs/common";
import {
  ApiBody,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { medication, Prisma } from "@prisma/client";
import { MedicationRepository } from "@repositories/medications.repository";
import { MedicationRequest } from "src/models/requests/medication.request";

@ApiTags("Medication")
@Controller("medication")
export class MedicationController {
  constructor(private medication: MedicationRepository) {}

  @ApiOperation({
    summary: "Get all medications.",
  })
  @ApiResponse({ type: [MedicationRequest] })
  @Get("/all")
  async getAll(): Promise<medication[]> {
    return await this.medication.getAll({ take: 100 });
  }

  @ApiOperation({
    summary: "Get medication.",
  })
  @ApiQuery(
    {
      name: "RXCUI",
      required: false
    }
  )
  @ApiQuery(
    {
      name: "STR",
      required: false
    }
  )
  @ApiResponse({ type: MedicationRequest })
  @Get()
  async getOne(
    @Query() data: { RXCUI: string; STR?: string }
  ): Promise<medication[]> {
    return await this.medication.getOne(data);
  }

  @ApiOperation({
    summary: "Post new medication.",
  })
  @ApiBody({ type: MedicationRequest })
  @Post()
  async create(@Body() data: MedicationRequest): Promise<medication> {
    return await this.medication.create(data);
  }
}
