import { Body, Controller, Get, Post, Query } from "@nestjs/common";
import {
  ApiBody,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { observations, Prisma } from "@prisma/client";
import { ObservationsRepository } from "@repositories/observations.repository";
import { SnomedRepository } from "@repositories/snomed.repository";
import { ObservationsRequest } from "src/models/requests/observation.request";
import { SnomedRequest } from "src/models/requests/snomed.request";
import { GetSomeObservation } from "src/models/responses/getSomeObservation.response";

@ApiTags("Snomed")
@Controller("snomed")
export class SnomedController {
  constructor(private snomed: SnomedRepository) {}

  @ApiOperation({
    summary: "Get all term that matches with term requested",
  })
  @ApiQuery({
    name: "term",
    required: false,
  })
  @ApiResponse({ type: SnomedRequest })
  @Get('/related')
  async getSome(@Query() data: { term: string }): Promise<SnomedRequest> {
    const res = await this.snomed.getOne(data);
    const json = JSON.stringify(res, (_, v) =>
      typeof v === "bigint" ? v.toString() : v
    );
    return JSON.parse(json);
  }

  @ApiOperation({
    summary: "Get by moduleid or conceptid",
  })
  @ApiQuery({
    name: "id",
    required: false,
  })
  @ApiResponse({ type: SnomedRequest })
  @Get()
  async get(@Query() data: { id: string }): Promise<SnomedRequest> {
    const res = await this.snomed.getByModule(data);
    const json = JSON.stringify(res, (_, v) =>
      typeof v === "bigint" ? v.toString() : v
    );
    return JSON.parse(json);
  }
}
