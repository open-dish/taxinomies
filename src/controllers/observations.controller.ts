import { Body, Controller, Get, Post, Query } from "@nestjs/common";
import {
  ApiBody,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { observations, Prisma } from "@prisma/client";
import { ObservationsRepository } from "@repositories/observations.repository";
import { ObservationsRequest } from "src/models/requests/observation.request";
import { GetSomeObservation } from "src/models/responses/getSomeObservation.response";

@ApiTags("Observation")
@Controller("observation")
export class ObservationsController {
  constructor(private observation: ObservationsRepository) {}

  @ApiOperation({
    summary: "Get all observations.",
  })
  @ApiResponse({ type: [ObservationsRequest] })
  @Get("all")
  async getAll(): Promise<observations[]> {
    return await this.observation.getAll({ take: 100 });
  }

  @ApiOperation({
    summary: "Get observation.",
  })
  @ApiQuery({
    name: "LOINC_NUM",
    required: false,
  })
  @ApiQuery({
    name: "COMPONENT",
    required: false,
  })
  @ApiResponse({ type: ObservationsRequest })
  @Get()
  async getOne(
    @Query() data: { LOINC_NUM: string, COMPONENT: string }
  ): Promise<observations[]> {
    return await this.observation.getOne(data);
  }

  @ApiOperation({
    summary: "Get all data related with the keyword.",
  })
  @ApiQuery({
    name: "keyword",
    required: false,
  })
  @ApiResponse({ type: GetSomeObservation })
  @Get('/related')
  async getSome(
    @Query() data: { keyword: string }
  ): Promise<any> {
    return await this.observation.getSome(data);
  }

  @ApiOperation({
    summary: "Post new observation.",
  })
  @ApiBody({ type: ObservationsRequest })
  @Post()
  async create(@Body() data: ObservationsRequest): Promise<observations> {
    return await this.observation.create(data);
  }
}
