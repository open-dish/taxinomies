function getEnvOrThrow(envName: string): string {
  const env = process.env[envName];
  if (!env) `Missing environment variable ${envName}`;
  return env;
}

export const APP_PORT = getEnvOrThrow('APP_PORT');
export const PRISMA_HOST = getEnvOrThrow('PRISMA_HOST');
export const PRISMA_PORT = getEnvOrThrow('PRISMA_PORT');
export const PRISMA_USERNAME = getEnvOrThrow('PRISMA_USERNAME');
export const PRISMA_PASSWORD = getEnvOrThrow('PRISMA_PASSWORD');
export const PRISMA_DATABASE = getEnvOrThrow('PRISMA_DATABASE');
