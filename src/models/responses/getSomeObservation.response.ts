import { PickType } from "@nestjs/swagger";
import { ObservationsRequest } from "../requests/observation.request";

export class GetSomeObservation extends PickType(ObservationsRequest, [
  "LOINC_NUM",
  "COMPONENT",
  "LONG_COMMON_NAME",
  "RELATEDNAMES2",
  "SYSTEM1",
  "DefinitionDescription",
  "SHORTNAME",
  "DisplayName",
] as const) {}
