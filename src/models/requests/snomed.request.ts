import { ApiProperty } from '@nestjs/swagger';

export class SnomedRequest {
  @ApiProperty()
  effectiveTime: Date;
  @ApiProperty()
  active: number;
  @ApiProperty()
  moduleId: string;
  @ApiProperty()
  conceptId: string;
  @ApiProperty()
  languageCode: string;
  @ApiProperty()
  typeId: string;
  @ApiProperty()
  term: string;
  @ApiProperty()
  caseSignificanceId: string;
}

