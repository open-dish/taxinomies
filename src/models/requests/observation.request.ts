import { ApiProperty } from '@nestjs/swagger';

export class ObservationsRequest {
  @ApiProperty()
  LOINC_NUM: string;
  @ApiProperty()
  COMPONENT: string;
  @ApiProperty()
  PROPERTY: string;
  @ApiProperty()
  TIME_ASPCT: string;
  @ApiProperty()
  SYSTEM1: string;
  @ApiProperty()
  SCALE_TYP: string;
  @ApiProperty()
  METHOD_TYP: string;
  @ApiProperty()
  VersionLastChanged: string;
  @ApiProperty()
  DefinitionDescription: string;
  @ApiProperty()
  STATUS1: string;
  @ApiProperty()
  CONSUMER_NAME: string;
  @ApiProperty()
  CLASSTYPE: string;
  @ApiProperty()
  FORMULA: string;
  @ApiProperty()
  EXMPL_ANSWERS: string;
  @ApiProperty()
  SURVEY_QUEST_TEXT: string;
  @ApiProperty()
  SURVEY_QUEST_SRC: string;
  @ApiProperty()
  UNITSREQUIRED: string;
  @ApiProperty()
  RELATEDNAMES2: string;
  @ApiProperty()
  SHORTNAME: string;
  @ApiProperty()
  ORDER_OBS: string;
  @ApiProperty()
  HL7_FIELD_SUBFIELD_ID: string;
  @ApiProperty()
  EXTERNAL_COPYRIGHT_NOTICE: string;
  @ApiProperty()
  EXAMPLE_UNITS: string;
  @ApiProperty()
  LONG_COMMON_NAME: string;
  @ApiProperty()
  EXAMPLE_UCUM_UNITS: string;
  @ApiProperty()
  STATUS_REASON: string;
  @ApiProperty()
  STATUS_TEXT: string;
  @ApiProperty()
  CHANGE_REASON_PUBLIC: string;
  @ApiProperty()
  COMMON_TEST_RANK: string;
  @ApiProperty()
  COMMON_ORDER_RANK: string;
  @ApiProperty()
  COMMON_SI_TEST_RANK: string;
  @ApiProperty()
  HL7_ATTACHMENT_STRUCTURE: string;
  @ApiProperty()
  EXTERNAL_COPYRIGHT_LINK: string;
  @ApiProperty()
  PanelType: string;
  @ApiProperty()
  AskAtOrderEntry: string;
  @ApiProperty()
  AssociatedObservations: string;
  @ApiProperty()
  VersionFirstReleased: string;
  @ApiProperty()
  ValidHL7AttachmentRequest: string;
  @ApiProperty()
  DisplayName: string;
  
}

