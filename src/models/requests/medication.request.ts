import { ApiProperty } from '@nestjs/swagger';

export class MedicationRequest {
  @ApiProperty()
  RXCUI: string;
  @ApiProperty()
  LAT: string;
  @ApiProperty()
  TS: string;
  @ApiProperty()
  LUI: string;
  @ApiProperty()
  STT: string;
  @ApiProperty()
  SUI: string;
  @ApiProperty()
  ISPREF: string;
  @ApiProperty()
  RXAUI: string;
  @ApiProperty()
  SAUI: string;
  @ApiProperty()
  SCUI: string;
  @ApiProperty()
  SDUI: string;
  @ApiProperty()
  SAB: string;
  @ApiProperty()
  TTY: string;
  @ApiProperty()
  CODE: string;
  @ApiProperty()
  STR: string;
  @ApiProperty()
  SRL: string;
  @ApiProperty()
  SUPPRESS: string;
  @ApiProperty()
  CVF: string;
}

