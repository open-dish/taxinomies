FROM node:18-buster AS v1-build

COPY . /build
WORKDIR /build
COPY package*.json ./
RUN yarn install
COPY env.example .env
EXPOSE 3000

CMD ["yarn", "start:dev"]


