## Description

<b> Mc857 project </b>

## Requirements

- Install [node](https://nodejs.org/en/download/) <b> version 14 </b>
- Install [yarn](https://classic.yarnpkg.com/en/docs/install/#debian-stable)
- Install [docker](https://docs.docker.com/install/)
- Install [docker-compose](https://docs.docker.com/compose/install/)

### Prepare your development environment

Create a copy .env file from .env.example and populate the variables.

Build and install the dependencies:

## Installation with docker

```bash
# Navigate to the root of the project and run on a terminal

$ make up # up the docker image from the database

$ npx prisma migrate dev # installs pending database migrations, it is only necessary the first time

$ sh build.sh # build container api

```

## Running the app local

```bash
# Navigate to the root of the project and run on a terminal
$ yarn # installs project dependencies

$ make up

$ npx prisma migrate dev # installs pending database migrations, it is only necessary the first time

# development
$ yarn start # execute the start script in package.json, start the project

# watch mode
$ yarn start:dev # execute the project in watch mode

# production mode
$ yarn start:prod  # executes the project for production
```