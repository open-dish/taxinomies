/*
  Warnings:

  - You are about to drop the `config_settings` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `snap_concept` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `snap_description` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `snap_refset_language` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `snap_relationship` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `snap_transclose` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "config_settings";

-- DropTable
DROP TABLE "snap_concept";

-- DropTable
DROP TABLE "snap_description";

-- DropTable
DROP TABLE "snap_refset_language";

-- DropTable
DROP TABLE "snap_relationship";

-- DropTable
DROP TABLE "snap_transclose";

-- CreateTable
CREATE TABLE "config" (
    "id" SERIAL NOT NULL,
    "languageId" BIGINT NOT NULL,
    "languageName" TEXT NOT NULL,
    "snapshotTime" TIMESTAMP(3) NOT NULL,
    "deltaStartTime" TIMESTAMP(3) NOT NULL,
    "deltaEndTime" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "config_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "concept" (
    "id" BIGSERIAL NOT NULL,
    "effectiveTime" TIMESTAMP(3) NOT NULL,
    "active" INTEGER NOT NULL,
    "moduleId" BIGINT NOT NULL,
    "definitionStatusId" BIGINT NOT NULL,

    CONSTRAINT "concept_pkey" PRIMARY KEY ("id","effectiveTime")
);

-- CreateTable
CREATE TABLE "description" (
    "id" BIGSERIAL NOT NULL,
    "effectiveTime" TIMESTAMP(3) NOT NULL,
    "active" INTEGER NOT NULL,
    "moduleId" BIGINT NOT NULL,
    "conceptId" BIGINT NOT NULL,
    "languageCode" TEXT NOT NULL,
    "typeId" BIGINT NOT NULL,
    "term" TEXT NOT NULL,
    "caseSignificanceId" BIGINT NOT NULL,

    CONSTRAINT "description_pkey" PRIMARY KEY ("id","effectiveTime")
);

-- CreateTable
CREATE TABLE "rf" (
    "id" TEXT NOT NULL,
    "effectiveTime" TIMESTAMP(3) NOT NULL,
    "active" INTEGER NOT NULL,
    "moduleId" BIGINT NOT NULL,
    "refsetId" BIGINT NOT NULL,
    "referencedComponentId" BIGINT NOT NULL,
    "acceptabilityId" BIGINT NOT NULL,

    CONSTRAINT "rf_pkey" PRIMARY KEY ("id","effectiveTime")
);

-- CreateTable
CREATE TABLE "relationship" (
    "id" BIGSERIAL NOT NULL,
    "effectiveTime" TIMESTAMP(3) NOT NULL,
    "active" INTEGER NOT NULL,
    "moduleId" BIGINT NOT NULL,
    "sourceId" BIGINT NOT NULL,
    "destinationId" BIGINT NOT NULL,
    "relationshipGroup" INTEGER NOT NULL,
    "typeId" BIGINT NOT NULL,
    "characteristicTypeId" BIGINT NOT NULL,
    "modifierId" BIGINT NOT NULL,

    CONSTRAINT "relationship_pkey" PRIMARY KEY ("id","effectiveTime")
);

-- CreateTable
CREATE TABLE "transclose" (
    "subtypeId" BIGINT NOT NULL,
    "supertypeId" BIGINT NOT NULL,

    CONSTRAINT "transclose_pkey" PRIMARY KEY ("subtypeId","supertypeId")
);
