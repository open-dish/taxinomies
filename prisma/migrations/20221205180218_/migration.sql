-- CreateTable
CREATE TABLE "config_settings" (
    "id" SERIAL NOT NULL,
    "languageId" BIGINT NOT NULL,
    "languageName" TEXT NOT NULL,
    "snapshotTime" TIMESTAMP(3) NOT NULL,
    "deltaStartTime" TIMESTAMP(3) NOT NULL,
    "deltaEndTime" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "config_settings_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "snap_concept" (
    "id" BIGSERIAL NOT NULL,
    "effectiveTime" TIMESTAMP(3) NOT NULL,
    "active" INTEGER NOT NULL,
    "moduleId" BIGINT NOT NULL,
    "definitionStatusId" BIGINT NOT NULL,
    "deltaEndTime" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "snap_concept_pkey" PRIMARY KEY ("id","effectiveTime")
);

-- CreateTable
CREATE TABLE "snap_description" (
    "id" BIGSERIAL NOT NULL,
    "effectiveTime" TIMESTAMP(3) NOT NULL,
    "active" INTEGER NOT NULL,
    "moduleId" BIGINT NOT NULL,
    "conceptId" BIGINT NOT NULL,
    "languageCode" TEXT NOT NULL,
    "typeId" BIGINT NOT NULL,
    "term" TEXT NOT NULL,
    "caseSignificanceId" BIGINT NOT NULL,

    CONSTRAINT "snap_description_pkey" PRIMARY KEY ("id","effectiveTime")
);

-- CreateTable
CREATE TABLE "snap_refset_language" (
    "id" BIGSERIAL NOT NULL,
    "effectiveTime" TIMESTAMP(3) NOT NULL,
    "active" INTEGER NOT NULL,
    "moduleId" BIGINT NOT NULL,
    "refsetId" BIGINT NOT NULL,
    "referencedComponentId" BIGINT NOT NULL,
    "acceptabilityId" BIGINT NOT NULL,

    CONSTRAINT "snap_refset_language_pkey" PRIMARY KEY ("id","effectiveTime")
);

-- CreateTable
CREATE TABLE "snap_relationship" (
    "id" BIGSERIAL NOT NULL,
    "effectiveTime" TIMESTAMP(3) NOT NULL,
    "active" INTEGER NOT NULL,
    "moduleId" BIGINT NOT NULL,
    "sourceId" BIGINT NOT NULL,
    "destinationId" BIGINT NOT NULL,
    "relationshipGroup" INTEGER NOT NULL,
    "typeId" BIGINT NOT NULL,
    "characteristicTypeId" BIGINT NOT NULL,
    "modifierId" BIGINT NOT NULL,

    CONSTRAINT "snap_relationship_pkey" PRIMARY KEY ("id","effectiveTime")
);

-- CreateTable
CREATE TABLE "snap_transclose" (
    "subtypeId" BIGINT NOT NULL,
    "supertypeId" BIGINT NOT NULL,

    CONSTRAINT "snap_transclose_pkey" PRIMARY KEY ("subtypeId","supertypeId")
);
