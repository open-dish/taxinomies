/*
  Warnings:

  - You are about to drop the column `deltaEndTime` on the `snap_concept` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "snap_concept" DROP COLUMN "deltaEndTime";
