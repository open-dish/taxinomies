/*
  Warnings:

  - You are about to drop the `concept` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `config` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `description` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `relationship` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `rf` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `transclose` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "concept";

-- DropTable
DROP TABLE "config";

-- DropTable
DROP TABLE "description";

-- DropTable
DROP TABLE "relationship";

-- DropTable
DROP TABLE "rf";

-- DropTable
DROP TABLE "transclose";

-- CreateTable
CREATE TABLE "config_settings" (
    "id" SERIAL NOT NULL,
    "languageId" BIGINT NOT NULL,
    "languageName" TEXT NOT NULL,
    "snapshotTime" TIMESTAMP(3) NOT NULL,
    "deltaStartTime" TIMESTAMP(3) NOT NULL,
    "deltaEndTime" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "config_settings_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "snap_description" (
    "id" BIGSERIAL NOT NULL,
    "effectiveTime" TIMESTAMP(3) NOT NULL,
    "active" INTEGER NOT NULL,
    "moduleId" BIGINT NOT NULL,
    "conceptId" BIGINT NOT NULL,
    "languageCode" TEXT NOT NULL,
    "typeId" BIGINT NOT NULL,
    "term" TEXT NOT NULL,
    "caseSignificanceId" BIGINT NOT NULL,

    CONSTRAINT "snap_description_pkey" PRIMARY KEY ("id","effectiveTime")
);
