/*
  Warnings:

  - The primary key for the `snap_refset_language` table will be changed. If it partially fails, the table could be left without primary key constraint.

*/
-- AlterTable
ALTER TABLE "snap_refset_language" DROP CONSTRAINT "snap_refset_language_pkey",
ALTER COLUMN "id" SET DEFAULT '',
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ADD CONSTRAINT "snap_refset_language_pkey" PRIMARY KEY ("id", "effectiveTime");
DROP SEQUENCE "snap_refset_language_id_seq";
